

package priv.base.modules.oss.service;

import priv.base.common.page.PageData;
import priv.base.common.service.BaseService;
import priv.base.modules.oss.entity.SysOssEntity;

import java.util.Map;

/**
 * 文件上传
 * 
 * @author Mike
 */
public interface SysOssService extends BaseService<SysOssEntity> {

	PageData<SysOssEntity> page(Map<String, Object> params);
}

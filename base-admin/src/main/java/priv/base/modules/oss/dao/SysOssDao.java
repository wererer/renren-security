

package priv.base.modules.oss.dao;

import priv.base.common.dao.BaseDao;
import priv.base.modules.oss.entity.SysOssEntity;
import org.apache.ibatis.annotations.Mapper;

/**
 * 文件上传
 * 
 * @author Mike
 */
@Mapper
public interface SysOssDao extends BaseDao<SysOssEntity> {
	
}

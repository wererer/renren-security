

package priv.base.modules.job.service;

import priv.base.common.page.PageData;
import priv.base.common.service.BaseService;
import priv.base.modules.job.dto.ScheduleJobLogDTO;
import priv.base.modules.job.entity.ScheduleJobLogEntity;

import java.util.Map;

/**
 * 定时任务日志
 *
 * @author Mike
 */
public interface ScheduleJobLogService extends BaseService<ScheduleJobLogEntity> {

	PageData<ScheduleJobLogDTO> page(Map<String, Object> params);

	ScheduleJobLogDTO get(Long id);
}

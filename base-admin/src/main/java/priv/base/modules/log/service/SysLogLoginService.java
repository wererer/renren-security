

package priv.base.modules.log.service;

import priv.base.common.page.PageData;
import priv.base.common.service.BaseService;
import priv.base.modules.log.dto.SysLogLoginDTO;
import priv.base.modules.log.entity.SysLogLoginEntity;

import java.util.List;
import java.util.Map;

/**
 * 登录日志
 *
 * @author Mike
 * @since 1.0.0
 */
public interface SysLogLoginService extends BaseService<SysLogLoginEntity> {

    PageData<SysLogLoginDTO> page(Map<String, Object> params);

    List<SysLogLoginDTO> list(Map<String, Object> params);

    void save(SysLogLoginEntity entity);
}
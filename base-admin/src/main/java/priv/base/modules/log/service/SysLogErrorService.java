

package priv.base.modules.log.service;


import priv.base.common.page.PageData;
import priv.base.common.service.BaseService;
import priv.base.modules.log.dto.SysLogErrorDTO;
import priv.base.modules.log.entity.SysLogErrorEntity;

import java.util.List;
import java.util.Map;

/**
 * 异常日志
 *
 * @author Mike
 * @since 1.0.0
 */
public interface SysLogErrorService extends BaseService<SysLogErrorEntity> {

    PageData<SysLogErrorDTO> page(Map<String, Object> params);

    List<SysLogErrorDTO> list(Map<String, Object> params);

    void save(SysLogErrorEntity entity);

}
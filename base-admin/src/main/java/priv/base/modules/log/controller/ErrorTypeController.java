

package priv.base.modules.log.controller;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import lombok.SneakyThrows;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import priv.base.common.annotation.LogOperation;
import priv.base.common.constant.Constant;
import priv.base.common.exception.ErrorCode;
import priv.base.common.exception.RenException;
import priv.base.common.page.PageData;
import priv.base.common.utils.ExcelUtils;
import priv.base.common.utils.Result;
import priv.base.modules.log.dto.SysLogOperationDTO;
import priv.base.modules.log.excel.SysLogOperationExcel;
import priv.base.modules.log.service.SysLogOperationService;
import springfox.documentation.annotations.ApiIgnore;

import javax.servlet.http.HttpServletResponse;
import java.util.List;
import java.util.Map;


/**
 * 异常类型处理
 *
 * @author Mike
 * @since 1.0.0
 */
@RestController
@RequestMapping("sys/log/errortype")
@Api(tags="异常类型处理")
public class ErrorTypeController {
    @Autowired
    private SysLogOperationService sysLogOperationService;

    @GetMapping("RenException1")
    @ApiOperation("RenException1")
    public Result RenException1(@ApiIgnore @RequestParam Map<String, Object> params){
        throw new RenException("测试自定义异常");
    }

    @SneakyThrows
    @GetMapping("")
    @ApiOperation("exceptionception")
    public Result exception(@ApiIgnore @RequestParam Map<String, Object> params){
        throw new Exception("测试Exception");
    }

    @GetMapping("RenException2")
    @ApiOperation("RenException2")
    public Result RenException2(@ApiIgnore @RequestParam Map<String, Object> params){
        throw new RenException(ErrorCode.SUPERIOR_DEPT_ERROR);
    }




}
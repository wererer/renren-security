

package priv.base.modules.log.service;

import priv.base.common.page.PageData;
import priv.base.common.service.BaseService;
import priv.base.modules.log.dto.SysLogOperationDTO;
import priv.base.modules.log.entity.SysLogOperationEntity;

import java.util.List;
import java.util.Map;

/**
 * 操作日志
 *
 * @author Mike
 * @since 1.0.0
 */
public interface SysLogOperationService extends BaseService<SysLogOperationEntity> {

    PageData<SysLogOperationDTO> page(Map<String, Object> params);

    List<SysLogOperationDTO> list(Map<String, Object> params);

    void save(SysLogOperationEntity entity);
}
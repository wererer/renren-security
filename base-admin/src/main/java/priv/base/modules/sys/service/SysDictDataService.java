

package priv.base.modules.sys.service;

import priv.base.common.page.PageData;
import priv.base.common.service.BaseService;
import priv.base.modules.sys.dto.SysDictDataDTO;
import priv.base.modules.sys.entity.SysDictDataEntity;

import java.util.Map;

/**
 * 数据字典
 *
 * @author Mike
 */
public interface SysDictDataService extends BaseService<SysDictDataEntity> {

    PageData<SysDictDataDTO> page(Map<String, Object> params);

    SysDictDataDTO get(Long id);

    void save(SysDictDataDTO dto);

    void update(SysDictDataDTO dto);

    void delete(Long[] ids);

}


package priv.base.modules.sys.service;

import priv.base.common.page.PageData;
import priv.base.common.service.BaseService;
import priv.base.modules.sys.dto.SysDictTypeDTO;
import priv.base.modules.sys.entity.DictType;
import priv.base.modules.sys.entity.SysDictTypeEntity;

import java.util.List;
import java.util.Map;

/**
 * 数据字典
 *
 * @author Mike
 */
public interface SysDictTypeService extends BaseService<SysDictTypeEntity> {

    PageData<SysDictTypeDTO> page(Map<String, Object> params);

    SysDictTypeDTO get(Long id);

    void save(SysDictTypeDTO dto);

    void update(SysDictTypeDTO dto);

    void delete(Long[] ids);

    /**
     * 获取所有字典
     */
    List<DictType> getAllList();

}
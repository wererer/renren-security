

package priv.base.modules.sys.dao;

import priv.base.common.dao.BaseDao;
import priv.base.modules.sys.entity.SysRoleEntity;
import org.apache.ibatis.annotations.Mapper;

/**
 * 角色管理
 * 
 * @author Mike
 */
@Mapper
public interface SysRoleDao extends BaseDao<SysRoleEntity> {
	

}

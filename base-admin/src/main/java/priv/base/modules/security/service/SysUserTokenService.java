

package priv.base.modules.security.service;

import priv.base.common.service.BaseService;
import priv.base.common.utils.Result;
import priv.base.modules.security.entity.SysUserTokenEntity;

/**
 * 用户Token
 * 
 * @author Mike
 */
public interface SysUserTokenService extends BaseService<SysUserTokenEntity> {

	/**
	 * 生成token
	 * @param userId  用户ID
	 */
	Result createToken(Long userId);

	/**
	 * 退出，修改token值
	 * @param userId  用户ID
	 */
	void logout(Long userId);

}
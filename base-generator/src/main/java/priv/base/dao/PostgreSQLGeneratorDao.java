

package priv.base.dao;

import org.apache.ibatis.annotations.Mapper;

/**
 * PostgreSQL代码生成器
 *
 * @author Mike
 */
@Mapper
public interface PostgreSQLGeneratorDao extends GeneratorDao {

}



package priv.base.dao;

import org.apache.ibatis.annotations.Mapper;

/**
 * SQLServer代码生成器
 *
 * @author Mike
 */
@Mapper
public interface SQLServerGeneratorDao extends GeneratorDao {

}

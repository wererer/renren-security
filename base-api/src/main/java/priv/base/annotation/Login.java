

package priv.base.annotation;

import java.lang.annotation.*;

/**
 * 登录效验
 * @author Mike
 */
@Target(ElementType.METHOD)
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface Login {
}

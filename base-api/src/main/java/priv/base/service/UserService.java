

package priv.base.service;

import priv.base.common.service.BaseService;
import priv.base.entity.UserEntity;
import priv.base.dto.LoginDTO;

import java.util.Map;

/**
 * 用户
 * 
 * @author Mike
 */
public interface UserService extends BaseService<UserEntity> {

	UserEntity getByMobile(String mobile);

	UserEntity getUserByUserId(Long userId);

	/**
	 * 用户登录
	 * @param dto    登录表单
	 * @return        返回登录信息
	 */
	Map<String, Object> login(LoginDTO dto);
}

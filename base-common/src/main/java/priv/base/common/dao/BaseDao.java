

package priv.base.common.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * 基础Dao
 *
 * @author Mike
 * @since 1.0.0
 */
public interface BaseDao<T> extends BaseMapper<T> {

}
